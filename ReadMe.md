# ExciteMike.com

I use this to generate my website, <https://www.excitemike.com>, by using [mdbook] to render Markdown (specifically, [CommonMark]) as HTML.

[mdbook]: https://rust-lang.github.io/mdBook/
[CommonMark]: https://commonmark.org/

## Building

To build it locally, [install Rust], and then: 

```bash
$ git clone https://bitbucket.org/excitemike/mdbookwebsite.git
$ cd mdbookwebsite
$ cargo install mdbook
$ mdbook build
$ mdbook serve
```

[install Rust]: https://www.rust-lang.org/tools/install
