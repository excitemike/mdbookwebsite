# Games

## Punch the Red Ones Only

[ ![screenshot][1] ][2]

[1]: ./images/ptroo.png
[2]: ./prtoo.md

## You Have to Save Everyone but There Isn't Much Time!

[ ![screenshot][3] ][4]

[3]: ./images/yhtsebtimt.png
[4]: ./yhtsebtimt.md

## Lizard Split

[ ![screenshot][5] ][6]

[5]: https://www.excitemike.com/games/lizardsp/lizardsplit.png
[6]: ./lizardsplit.md

## DudeCollect

[ ![screenshot][7] ][8]

[7]: ./images/dudecollect.png
[8]: ./dudecollect.md

## Super Maria Cosmos

[ ![screenshot][9] ][10]

[9]: ./images/screenie_001.jpg
[10]: ./super_maria_cosmos.md

## No longer playable

[Games I made long ago that unfortunately aren't playable on today's computers](./no_longer_playable.md).
