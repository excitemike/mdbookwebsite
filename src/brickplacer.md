# Brickplacer

A small flash game made for Mini Ludum Dare #11 game jam. Now converted to HTML/JS. The theme was "Sandbox".

<style>.container{display:block;border:0;margin:auto;padding:0;overflow:hidden;height:320px}.container iframe{border:0;height:100%;width:100%;}</style><div class="container"><iframe src="https://www.excitemike.com/games/brickplacer/" allowfullscreen loading="lazy"></iframe></div>