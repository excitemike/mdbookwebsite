# Punch the Red Ones Only

A small flash game made for Mini Ludum Dare #10]. Now converted to HTML/JS.

The theme was "Domestic Violence", so... it's not very fun 😬

<style>.container{display:block;border:0;margin:auto;padding:0;overflow:hidden;height:320px}.container iframe{border:0;height:100%;width:100%;}</style><div class="container"><iframe src="/games/ptroo/" allowfullscreen loading="lazy"></iframe></div>