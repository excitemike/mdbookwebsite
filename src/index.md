# Recent Games

## SerpentRL

[ ![screenshot][1] ][2]

[1]: ./images/serpentrl.png
[2]: https://excitemike.itch.io/serpent-rl

This is a game I made for the [7-Day Roguelike](https://itch.io/jam/7drl-challenge-2020) event in 2020. You can play it [on itch.io](https://excitemike.itch.io/serpent-rl).

[See more games I made](games.md)

## Tools

- [Tools](tools.md)
   - [Tools for Blood on the Clocktower / clocktower.online](botctools.md)
