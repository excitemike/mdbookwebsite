# DudeCollect

A small flash game I made for gamejam.org, which I don't think exists anymore. Now converted to HTML5! It *almost* works on mobile.

<style>.dudecollectcontainer{display:block;border:0;margin:auto;padding:0;overflow:hidden;padding-top:128.57%;position:relative;}.dudecollectcontainer iframe{border:0;height:100%;left:0;top:0;position:absolute;width:100%;}</style><div class="dudecollectcontainer"><iframe src="https://www.excitemike.com/games/dudecollect_ts" allowfullscreen loading="lazy" class="dudecollect"></iframe></div>
