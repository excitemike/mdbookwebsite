# Blood on the Clocktower Tools

## Roles viewer

A page where you can enter a link to your clocktower.online json file and get a nice, mobile friendly view: [Roles Viewer][1].

[1]: https://www.bloodstar.xyz/viewroles/

## Bloodstar Clocktica

[ ![screenshot of Bloodstar Clocktica][2] ][3]

[2]: ./images/bloodstar.png
[3]: https://www.bloodstar.xyz

For folks who are interested in making homebrew for Blood on the Clocktower, I made this [web-based tool][4] for creating custom editions. Includes automatically processing icons into roughly the same style as official icons, creating an almanac, and hosting the images and script json for use with [Clocktower.online][5].

[4]: https://www.bloodstar.xyz/
[5]: https://www.clocktower.online