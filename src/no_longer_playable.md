# No Longer Playable

<style>
.gamegrid {
    display:grid;
    grid-template-columns: auto auto;
    grid-gap: 0.5em;
}
.gamegrid img {
    max-width: 200px;
    display: block;
    margin: auto;
}
.gamegrid div {
    border-top: 1px solid #333;
}
.gamegrid h2 {
    gird-column: 1 / 3;
}
@media only screen and (max-width:571px) {
    .gamegrid img {
        max-width: 35vw;
    }
}
@media only screen and (max-width:400px) {
    .gamegrid {
        display: unset;
    }
    .gamegrid img {
        max-width: 65vw;
    }
}
</style>

Due to how they were made, these games aren't really playable on today's computers.

<div class="gamegrid">

![screenshot](./images/eaterbig.jpg)

<div>

### Eater

Silly Flash game where you shuffle resources around to try and keep a hungry character from starving.

</div>

![screenshot](./images/GravBall.png)

<div>

### GravBall

Silly Flash game where you kinda juggle a ball Arkanoids-style but with a black hole gravitationally affecting its path.

</div>

![screenshot](./images/squestthumb.png)

<div>

### Squest

A game about a squid swinging around like spiderman made for a college class. Made in DarkBASIC.

</div>

![screenshot](./images/robotfactory.png)

<div>

### Robot Factory

A puzzle game a bit like Wario's Woods made for a college class. It was made in Macromedia Director.

</div>

![screenshot](./images/exterminatorbig.jpg)

<div>

### The Exterminator

This game was made for a class in college. The assignment was to make a clone of Space Invaders, Arkanoid, or Asteroids with some new twist. Our team made one game that was all three! Made in Macromedia Director.

</div>

![screenshot](./images/animalmatchup.png)

<div>

### Animal Matchup

Memory game made for a class. Made with Macromedia Director.

</div>

![screenshot](./images/z2.png)

<div>

### Zwischenzug

This was the first real videogame I made! It doesn't really work on today's computers, sadly :(

[Source code](https://bitbucket.org/excitemike/zwischenzug/src/master/)

</div>

</div>
