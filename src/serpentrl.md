# SerpentRL

<center>

[ ![screenshot](./images/serpentrl.png) ](https://excitemike.itch.io/serpent-rl)

This is a game I made for the [7-Day Roguelike](https://itch.io/jam/7drl-challenge-2020) event in 2020. You can play it [on itch.io](https://excitemike.itch.io/serpent-rl).

</center>
