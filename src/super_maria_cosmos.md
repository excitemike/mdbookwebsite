# Super Maria Cosmos

![screenshot](https://www.excitemike.com/images/screenie_000.jpg) ![screenshot](https://www.excitemike.com/images/screenie_001.jpg)

A very short game I made for a ["Bootleg Demakes" themed competition on the old TIGSource forums](https://forums.tigsource.com/index.php?board=22.0). The concept was "Mario Galaxy demade for the Nintendo DS".

[Download](https://www.excitemike.com/games/SuperMaria.zip)
