# Lizard Split

<center>

![screenshot](https://www.excitemike.com/games/lizardsp/lizardsplit.png)

</center>

This was my first Klik of the Month Klub game! Made for [Klik of the Month Klub #24](http://www.glorioustrainwrecks.com/node/336).

Originally a 16-bit EXE, this bundles it with DosBox+Win3.1 so you can play it on Windows machines of circa 2021.

[Download lizardSplitDosBox.zip](https://www.excitemike.com/games/lizardsp/lizardSplitDosBox.zip)
